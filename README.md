# jerboa-infra

This repository contains all the configuration files for jerboa deployment.

You can find the jerboa source code at [this link](https://gitlab.com/Zenor27/jerboa-src).

## How to deploy ?

You need to have a k8s running on your system.  
This README will assume that you have microk8s or minikube installed.  
In the following commands, `mk` refers to an alias on `{microk8s|minikube} kubectl`, I advise you to add it in your `.bashrc`.

You need to set up PostgreSQL zalando operator on your system.  
You need to set up ArgoCD on your system.

### Addons


#### Microk8s

You must enable the following addons on your system:

```bash
42sh~ microk8s enable dns
42sh~ microk8s enable ingress
42sh~ microk8s enable storage
42sh~ mk apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/baremetal/deploy.yaml
```

#### Minikube

You must enable the following addons on your system:

```bash
42sh~ minikube addons enable ingress
42sh~ minikube addons enable ingress-dns
42sh~ minikube addons enable storage-provisioner

```


### Namespace

You must create the namespace that will contain all of our deployment:

```bash
42sh~ mk apply -f base/namespace
```

### PostgreSQL setup

To set up PostgreSQL, do the following commands:

```bash
42sh~ mk apply -k github.com/zalando/postgres-operator/manifests
# Now wait/verify that the postgresql operator is running
42sh~ mk get pod -l name=postgres-operator
```

Deploy the UI:
```bash
42sh~ mk apply -k github.com/zalando/postgres-operator/ui/manifests
# Now wait/verify that the UI operator is running
42sh~ mk get pod -l name=postgres-operator-ui 
```

To access PostgreSQL UI, you need port-forwarding to expose the service.

```bash
42sh~ mk port-forward svc/postgres-operator-ui 8081:80
```

The UI is now available on `localhost:8081`.

### Create a pgsql cluster

#### Automatic

We provide you a `database.yml` in the `base` folder that create the cluster automatically with the right settings.

```bash
42sh~ mk apply -f base/database.yml
```

#### Manual

In the UI, create a cluster:

* Name: `jerboa-cluster`
* Create a user: `jerboa`
* Create a database: `jerboa` with owner `jerboa`
* You can select the number of replicas, how master should behave. If you don't know what you are doing, just leave the defaults.

=> Create the cluster.

### Access the cluster on your host

```bash
# get name of master pod of acid-jerboa-cluster
42sh~ export PGMASTER=$(mk get pods -o jsonpath={.items..metadata.name} -l application=spilo,cluster-name=acid-jerboa-cluster,spilo-role=master)
# set up port forward
42sh~ mk port-forward $PGMASTER 6432:5432
```

The database cluster will be available on the port `6432` on your host.

### Get password of the cluster

```bash
# get the password of acid-jerboa-cluster
42sh~ export PGPASSWORD=$(mk get secret postgres.acid-jerboa-cluster.credentials -o 'jsonpath={.data.password}' | base64 -d)
```

### Secret file

You must apply the secret file in order for the pods to access the database cluster.

```bash
42sh~ echo -n $PGPASSWORD | base64
# replace the FIXME in base/secret.yml by the result
42sh~ mk apply -f base/secret.yml
```

### ArgoCD setup

To set up ArgoCD, do the following commands:

```bash
42sh~ mk create namespace argocd
42sh~ mk apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
# Wait for your pods to run...
# You can check pods status with `mk get pods -n argocd`
```
To access ArgoCD UI, you need port-forwarding to expose the service.

```bash
42sh~ mk port-forward svc/argocd-server -n argocd 8080:443
```

The UI is now available on `localhost:8080`.

The credentials will be:  
* username: `admin`
* password: the result of this command: `mk get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2`


Congratulations, ArgoCD is now fully working on your system.

### Create application

#### Automatic

We provide you a `argo.yml` in the `base` folder that create the application in argo automatically with the right settings.

```bash
42sh~ mk apply -f base/argo.yml
```

#### Manual

In order to deploy jerboa on your system, you will need to follow these steps:
* Create a new app in ArgoCD UI
* Name it as you want, `jerboa` should be fine.
* Project is `default`.
* Sync policy `Automatic`, it will automatically sync your cluster with the latest version of jerboa.
* You can check `Prune resources`, `Self heal` and `Auto-create namespace` if you want, I advise you to leave it unchecked if it is your first time with k8s or ArgoCD.
* Repository URL should be `https://gitlab.com/Zenor27/jerboa-infra.git`.
* The path should be `.`.
* Cluster URL should be: `https://kubernetes.default.svc`
* You can now create the application.

Since you set the sync policy to `Automatic`, it will deploy jerboa right after the creation. Otherwise, you should sync manually.
